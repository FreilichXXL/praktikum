def blatt_legen():
    global mushroomX
    global mushroomY
    global counter
    global schlange
    global speed
    position = kara.getPosition()
    if kara.mushroomFront():
        world.setMushroom(mushroomX, mushroomY, False)
        schlange.insert(0,position)
        counter += 1
    elif len(schlange) > 0:
        letzter = schlange.pop(len(schlange)-1)
        world.setLeaf(letzter.x,letzter.y,False)
        schlange.insert(0,position)
    kara.move()
    if len(schlange) > 0:
        world.setLeaf(position.x,position.y,True)
    tools.sleep(speed)

counter = 0
schlange = []
mushroomX = 0
mushroomY = 0
position_1 = None
position_2 = None        
speed = tools.intInput("Wie schnell willst du es haben (20-300); 20 = schnell, 300 = langsam")
max_points = tools.intInput("Wie viele Punkte vielst du dir vorsetzten")
tools.showMessage("Du darfst nicht auf die Bl�tter kommen!")
score = 0
blatt_suche_1 = 1
breite = world.getSizeX() -2
hoehe = world.getSizeY() -2
while not (kara.onLeaf() or score == max_points):
    score += 1    
    while mushroomX == 0:
        mushroomX = tools.random(breite)
    while mushroomY == 0:
        mushroomY = tools.random(hoehe)
    world.setMushroom(mushroomX,mushroomY, True)
    while not (kara.onLeaf() or counter == 1):
        blatt_legen() 
    counter = 0
    tools.sleep(100)
    mushroomX = 0
    mushroomY = 0
if score == max_points:
    if not kara.onLeaf():
        tools.showMessage("Du hast es geschafft")
    else:
        tools.showMessage("Game Over")         
else:
    tools.showMessage("Game Over")        
kara.setPosition(1,1)
