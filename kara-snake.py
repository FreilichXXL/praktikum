def blatt_legen_1(): 
    global position_1
    global score
    global speed
    kara.putLeaf()
    position_1 = kara.getPosition()
    blatt_legen_2()
    kara.move()
    tools.sleep(speed)
    world.setLeaf(position_1.x, position_1.y, False)
def blatt_legen_2(): 
    global position_1
    global score
    global speed
    if score > 1:
        position_2 = kara.getPosition()
        if position_1.x != position_2.x:
            if position_1.x < position_2.x:
                world.setLeaf(position_1.x - 1, position_1.y, True)
            elif position_1.x > position_2.x:
                world.setLeaf(position_1.x + 1, position_1.y, True)
        elif position_1.y != position_2.y:
            if position_1.y < position_2.y:
                world.setLeaf(position_1.x, position_1.y - 1, True)
            elif position_1.y > position_2.y:
                world.setLeaf(position_1.x, position_1.y + 1, True)
position_1 = None
position_2 = None        
speed = tools.intInput("Wie schnell willst du es haben (20-300); 20 = schnell, 300 = langsam")
max_points = tools.intInput("Wie viele Punkte vielst du dir vorsetzten")
tools.showMessage("Du darfst nicht auf die Blätter kommen!")
score = 0
blatt_suche_1 = 1
breite = world.getSizeX() -1
hoehe = world.getSizeY() -1
while not (kara.onLeaf() or score == max_points):
    score += 1    
    while (mushroomX == 0 or mushroomX == breite):
        mushroomX = tools.random(breite)
    while (mushroomY == 0 or mushroomY == hoehe):
        mushroomY = tools.random(hoehe)
    world.setMushroom(mushroomX, mushroomY, True)
    while not (kara.mushroomFront() or kara.onLeaf()):
        blatt_legen_1() 
    world.setMushroom(mushroomX, mushroomY, False)
    tools.sleep(100)
    mushroomX = 0
    mushroomY = 0
if score == max_points:
    if not kara.onLeaf():
        tools.showMessage("Du hast es geschafft")
    else:
        tools.showMessage("Du hast leider verloren")         
else:
    tools.showMessage("Du hast leider verloren")        
kara.setPosition(1,1)