def baum_finden():
    breite = 25
    schritte = 1
    while not (kara.treeRight() or kara.treeLeft() or kara.treeFront()):
        if not schritte > breite:
            kara.move()
            schritte += 1
        else:
            kara.turnLeft()
            kara.move()
            kara.turnRight()
            schritte = 1
    position_bestimmen()
    eingang_suchen()

def position_bestimmen():
    if kara.treeLeft():
        kara.turnLeft()
    if kara.treeFront():
        kara.turnLeft()    
            
def eingang_suchen():
    while not (kara.treeRight() and kara.treeLeft()):
        while kara.treeRight():
            kara.move()
        kara.turnRight()
        kara.move()   

def umdrehen():
    kara.turnLeft()
    kara.turnLeft()  
    
def baum_ueberpruefen_1():
    kara.move()
    if  not (kara.treeRight() or kara.treeLeft()): #Zahl überprüfen
        kara.move()
        if kara.onLeaf(): #zeichen überprüfen
            zeichen_erkennen()
        else:
            kara.move()
            if kara.onLeaf():      #zeichen überprüfen
                umdrehen()
                kara.move()
                umdrehen()
                zeichen_erkennen() 
            else:
                umdrehen()   #wieder rausgehen
                kara.move()
                kara.move()
                kara.move()
                kara.move()
                kara.move()
                kara.turnRight()
    else:
        if kara.treeLeft():
            kara.turnRight()
            kara.move()
            kara.turnLeft()
            zahl_erkennen() #erste Zahl
        else:
            kara.turnLeft()
            kara.move()
            kara.turnRight()
            zahl_erkennen_2() #zweite Zahl
    
    
def gefundene_zahlen_ueberpruefen():
    if gefundene_zeichen == 3:
        baum_ueberpruefen_2()  #wenn bereits 3 Zahlen gefunden sind
    else:
        baum_ueberpruefen_1()  # wen eine oder keine Zahl gefunden wurde
        
def zahl_erkennen():           # erste Zahl
    global gefundene_zeichen
    global zahl
    global zahl_2
    if kara.onLeaf():
        zahl += 1
    bewegungsmuster_1()
    if kara.onLeaf():
        zahl += 2
    bewegungsmuster_2()
    if kara.onLeaf():
        zahl += 4
    bewegungsmuster_3()
    if kara.onLeaf():
        zahl += 8
    bewegungsmuster_1()
    if kara.onLeaf():
        zahl += 16
    bewegungsmuster_2()
    if kara.onLeaf():
        zahl += 32
    bewegungsmuster_3()
    if kara.onLeaf():
        zahl += 64
    kara.turnLeft()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnRight()
    gefundene_zeichen += 1

def zahl_erkennen_2():     #zweite Zahl
    global gefundene_zeichen
    global zahl
    global zahl_2
    if kara.onLeaf():
        zahl_2 += 1
    bewegungsmuster_1()
    if kara.onLeaf():
        zahl_2 += 2
    bewegungsmuster_2()
    if kara.onLeaf():
        zahl_2 += 4
    bewegungsmuster_3()
    if kara.onLeaf():
        zahl_2 += 8
    bewegungsmuster_1()
    if kara.onLeaf():
        zahl_2 += 16
    bewegungsmuster_2()
    if kara.onLeaf():
        zahl_2 += 32
    bewegungsmuster_3()
    if kara.onLeaf():
        zahl_2 += 64
    kara.turnRight()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnRight()
    gefundene_zeichen += 1
                                
def bewegungsmuster_1():
    kara.move()
    kara.turnRight()
    kara.move()
    
def bewegungsmuster_2():
    umdrehen()
    kara.move()
    kara.move()
    
def bewegungsmuster_3():
    umdrehen()
    kara.move()
    kara.turnLeft()
    kara.move()
    
def baum_ueberpruefen_2():       
    kara.move()
    if (kara.treeLeft() or kara.treeRight()):    
        umdrehen()   #rausgehen
        kara.move()
        kara.move()
        kara.move()
        kara.turnRight()
        hauptschleife()
    else:
        kara.move()
        if kara.onLeaf():
            umdrehen()      #rausgehen
            kara.move()
            kara.move()
            kara.move()
            kara.move()
            kara.turnRight()
            hauptschleife()
        else:
            kara.move()
            if kara.onLeaf():
                umdrehen()      #rausgehen
                kara.move()
                kara.move()
                kara.move()
                kara.move()
                kara.move()
                kara.turnRight()
                hauptschleife()
            else:
                umdrehen()
                kara.move()
                kara.move()
                kara.turnRight()
                kara.move()
                umdrehen()
                zahlen_schreiben()
            
            
            
            
            
def zahlen_schreiben():
    kara.move()
    kara.turnLeft()
    global zeichen
    global ergebnis
    global zahl
    global zahl_2
    if zahl == 119:   #zahlen umwandeln
        zahl = 0        #erste zahl                                                
    elif zahl == 36:
        zahl = 1
    elif zahl == 93:
        zahl = 2
    elif zahl == 109:
        zahl = 3
    elif zahl == 46:
        zahl = 4
    elif zahl == 107:
        zahl = 5
    elif zahl == 123:
        zahl = 6
    elif zahl == 37:
        zahl = 7
    elif zahl == 127:
        zahl = 8
    elif zahl == 111:
        zahl = 9
    if zahl_2 == 119:   #zweite Zahl
        zahl_2 = 0                                                        
    elif zahl_2 == 36:
        zahl_2 = 1
    elif zahl_2 == 93:
        zahl_2 = 2
    elif zahl_2 == 109:
        zahl_2 = 3
    elif zahl_2 == 46:
        zahl_2 = 4
    elif zahl_2 == 107:
        zahl_2 = 5
    elif zahl_2 == 123:
        zahl_2 = 6
    elif zahl_2 == 37:
        zahl_2 = 7
    elif zahl_2 == 127:
        zahl_2 = 8
    elif zahl_2 == 111:
        zahl_2 = 9   
    if zeichen == 0:
        ergebnis = zahl * zahl_2
    elif zeichen == 1:
        ergebnis = zahl - zahl_2
    elif zeichen == 2:
        ergebnis = zahl / zahl_2
    elif zeichen == 3:
        ergebnis = zahl + zahl_2
    if ergebnis == 0:
        null_schreiben()
        positionieren()
        null_schreiben()
    elif ergebnis == 1:
        null_schreiben()
        positionieren()
        eins_schreiben()
    elif ergebnis == 2:
        null_schreiben()
        positionieren()
        zwei_schreiben()
    elif ergebnis == 3:
        null_schreiben()
        positionieren()
        drei_schreiben()
    elif ergebnis == 4:
        null_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == 5:
        null_schreiben()
        positionieren()
        fuenf_schreiben()
    elif ergebnis == 6:
        null_schreiben()
        positionieren()
        sechs_schreiben()
    elif ergebnis == 7:
        null_schreiben()
        positionieren()
        sieben_schreiben()
    elif ergebnis == 8:
        null_schreiben()
        positionieren()
        acht_schreiben()
    elif ergebnis == 9:
        null_schreiben()
        positionieren()
        neun_schreiben()
    elif ergebnis == 10:
        eins_schreiben()
        positionieren()
        null_schreiben()
    elif ergebnis == 11:
        eins_schreiben()
        positionieren()
        eins_schreiben()
    elif ergebnis == 12:
        eins_schreiben()
        positionieren()
        zwei_schreiben()
    elif ergebnis == 13:
        eins_schreiben()
        positionieren()
        drei_schreiben()
    elif ergebnis == 14:
        eins_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == 15:
        eins_schreiben()
        positionieren()
        fuenf_schreiben()
    elif ergebnis == 16:
        eins_schreiben()
        positionieren()
        sechs_schreiben()
    elif ergebnis == 17:
        eins_schreiben()
        positionieren()
        sieben_schreiben()
    elif ergebnis == 18:
        eins_schreiben()
        positionieren()
        acht_schreiben()
    elif ergebnis == 20:
        zwei_schreiben()
        positionieren()
        null_schreiben()
    elif ergebnis == 21:
        zwei_schreiben()
        positionieren()
        eins_schreiben()
    elif ergebnis == 24:
        zwei_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == 25:
        zwei_schreiben()
        positionieren()
        fuenf_schreiben()
    elif ergebnis == 27:
        zwei_schreiben()
        positionieren()
        sieben_schreiben()
    elif ergebnis == 28:
        zwei_schreiben()
        positionieren()
        acht_schreiben()
    elif ergebnis == 30:
        drei_schreiben()
        positionieren()
        null_schreiben() 
    elif ergebnis == 32:
        drei_schreiben()
        positionieren()
        zwei_schreiben() 
    elif ergebnis == 35:
        drei_schreiben()
        positionieren()
        funef_schreiben() 
    elif ergebnis == 36:
        drei_schreiben()
        positionieren()
        sechs_schreiben() 
    elif ergebnis == 40:
        vier_schreiben()
        positionieren()
        null_schreiben() 
    elif ergebnis == 42:
        vier_schreiben()
        positionieren()
        zwei_schreiben()
    elif ergebnis == 45:
        vier_schreiben()
        positionieren()
        fuenf_schreiben()
    elif ergebnis == 48:
        vier_schreiben()
        positionieren()
        acht_schreiben()
    elif ergebnis == 49:
        vier_schreiben()
        positionieren()
        neun_schreiben()
    elif ergebnis == 54:
        fuenf_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == 56:
        fuenf_schreiben()
        positionieren()
        sechs_schreiben()
    elif ergebnis == 63:
        sechs_schreiben()
        positionieren()
        drei_schreiben()
    elif ergebnis == 64:
        sechs_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == 72:
        sieben_schreiben()
        positionieren()
        zwei_schreiben()
    elif ergebnis == 81:
        acht_schreiben()
        positionieren()
        eins_schreiben()
    elif ergebnis == -1:
        minus_schreiben()
        positionieren()
        eins_schreiben()
    elif ergebnis == -2:
        minus_schreiben()
        positionieren()
        zwei_schreiben()
    elif ergebnis == -3:
        minus_schreiben()
        positionieren()
        drei_schreiben()
    elif ergebnis == -4:
        minus_schreiben()
        positionieren()
        vier_schreiben()
    elif ergebnis == -5:
        minus_schreiben()
        positionieren()
        fuenf_schreiben()
    elif ergebnis == -6:
        minus_schreiben()
        positionieren()
        sechs_schreiben()
    elif ergebnis == -7:
        minus_schreiben()
        positionieren()
        sieben_schreiben()
    elif ergebnis == -8:
        minus_schreiben()
        positionieren()
        acht_schreiben()
    elif ergebnis == -9:
        minus_schreiben()
        positionieren()
        neun_schreiben()
                
def positionieren():
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    umdrehen()
    
def null_schreiben():
    kara.turnRight()
    kara.putLeaf()
    kara.move()
    kara.turnLeft() 
    lange_strecke_kurve(True)
    kurze_strecke_kurve(True)
    lange_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.turnLeft()
    
def eins_schreiben():           
    kara.turnLeft()
    kara.move()
    kara.turnRight()
    lange_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()

def zwei_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnLeft()
    kara.move()
    umdrehen()
    
def drei_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnLeft()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.move()
    umdrehen()
    
def vier_schreiben():
    kara.turnRight()
    kara.move()
    kara.turnLeft()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.removeLeaf()
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.turnLeft()
    
def fuenf_schreiben():
    kara.turnLeft()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    
def sechs_schreiben():
    kara.turnLeft()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(True)
    lange_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.turnLeft()
    kara.move()
    
def sieben_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()
    
def acht_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.putLeaf()
    kara.turnRight()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.turnLeft()
    
def neun_schreiben():
    kara.move()
    kara.move()
    kara.putLeaf()
    kara.turnRight()
    kara.move()
    kara.turnRight()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    
def minus_schreiben():
    kara.move()
    kara.move()
    kara.putLeaf()
    kara.turnLeft()
    kara.move()
    kara.putLeaf()
    umdrehen()
    kara.move()
    kara.move()
    umdrehen()
    kara.putLeaf()
    kara.move()
    
def rausgehen():
    umdrehen()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
   
    

def kurze_strecke_kurve(links):
    if links:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnLeft()
    else:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnRight()

def lange_strecke_kurve(links):
    if links:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnLeft()
    else:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnRight()
      
    
                                                                                                          
def hauptschleife():                                                                                                                             
    baum_finden()
    gefundene_zahlen_ueberpruefen()
    
def zeichen_erkennen():
    global zeichen
    global gefundene_zeichen
    if kara.onLeaf():
        zeichen += 1
    kara.turnRight()
    kara.move()
    kara.turnLeft()
    kara.move()
    if kara.onLeaf():
        zeichen += 1
    kara.move()
    kara.turnLeft()
    kara.move()
    if kara.onLeaf():
        zeichen += 1
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnRight()
    gefundene_zeichen += 1
    
    

zeichen = 0
zahl = 0 
zahl_2 = 0
ergebnis = 0
gefundene_zeichen = 0
while not gefundene_zeichen == 3:
    hauptschleife()
    tools.println("Gefundene Zeichen: " + str(gefundene_zeichen))
hauptschleife()
rausgehen()
umdrehen()
