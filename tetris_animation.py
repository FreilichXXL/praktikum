def spielfeld_setzen():
    for tree_1_y in range (0, 31):
        for tree_1_x in [0, 7, 8, 30]:
            world.setTree(tree_1_x,tree_1_y,True)
        tools.sleep(50)
    for tree_2_x in range (1, 7):
        for tree_2_y in [0, 1, 12, 13, 29, 30]:
            world.setTree(tree_2_x,tree_2_y,True)
            tools.sleep(10)
        tools.sleep(50)
    for tree_3_x in range (9, 30):
        for tree_3_y in [0, 6, 7, 30]:
            world.setTree(tree_3_x,tree_3_y,True)
        tools.sleep(50)
    for tree_4_x in range (22, 30):
        for tree_4_y in range (26, 31):
            world.setTree(tree_4_x,tree_4_y,True)
            tools.sleep(10)
        tools.sleep(50)
    for tree_5_y in range (8, 26):
        world.setTree(22,tree_5_y,True)
        tools.sleep(50)
    for tree_6_x in range (0, 31):
        world.setTree(tree_6_x,36,True)
        tools.sleep(30)
    for tree_7_y in range (31, 36):
        for tree_7_x in [0, 30]:
            world.setTree(tree_7_x,tree_7_y,True)
        tools.sleep(40)
    for leaf_1_x in [2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 26, 27, 28]:
        for leaf_1_y in [35, 31]:
            world.setLeaf(leaf_1_x,leaf_1_y,True)
            tools.sleep(5)
        tools.sleep(50)
    for leaf_2_x in range (2, 30,2):
        for leaf_2_y in range  (32, 35):
            world.setLeaf(leaf_2_x, leaf_2_y, True)
            tools.sleep(20)
        tools.sleep(50)
    for leaf_3_x in range (2, 5):
        for leaf_3_y in [16, 24]:
            world.setLeaf(leaf_3_x, leaf_3_y, True)
            tools.sleep(5)
        tools.sleep(40)
    for leaf_4_y in [2, 5, 8]:
        world.setLeaf(1, leaf_4_y, True)
        tools.sleep(30)
    for leaf_5_x in range (2, 6):   
        for leaf_5_y in range (3, 12, 3):
            world.setLeaf(leaf_5_x, leaf_5_y, True)
            tools.sleep(5)
        tools.sleep(30)
    for leaf_6_x in [23, 25, 26, 27, 28, 29]:
        for leaf_6_y in [9, 10, 11, 13, 14, 16, 17, 18, 19, 21, 23, 24, 25]:
            world.setLeaf(leaf_6_x, leaf_6_y, True)
            tools.sleep(20)
        tools.sleep(20)
    for leaf_7_x in [13, 14, 15, 17, 18, 19, 23, 24, 25]:
        for leaf_7_y in [1, 5]:
            world.setLeaf(leaf_7_x, leaf_7_y, True)
            tools.sleep(20)
    for leaf_8_x in range (13, 27,2):
        for leaf_8_y in [2, 4]:
            world.setLeaf(leaf_8_x, leaf_8_y, True)
            tools.sleep(20)
        tools.sleep(50)
    for leaf_9_x in [13, 15, 17, 19, 23, 25]:
        world.setLeaf(leaf_9_x, 3, True)
        tools.sleep(40)
    tools.sleep(500)   
    
    
    
    
world.clearAll()
world.setSize(31,37)
spielfeld_setzen() 
import time
numbers = [ 
    31599, # 0b0111101101101111
    4681,  # 0b0001001001001001
    29671, # 0b0111001111100111
    29647, # 0b0111001111001111
    23497, # 0b0101101111001001
    31183, # 0b0111100111001111
    31215, # 0b0111100111101111
    29257, # 0b0111001001001001
    31727, # 0b0111101111101111
    31695  # 0b0111101111001111
]

def paintNumber(number, xOffset = 12, yOffset = 0):
    global numbers
    tools.println(str(number))
    mask = 16384 # = 0b0100000000000000
    for y in range(1, 6):
        for x in range(1, 4):
	        visible = numbers[number] & mask
	        world.setLeaf(x + xOffset, y + yOffset, visible)
	        mask /= 2       

def paintDots():
    visible = not(world.isLeaf(21,2))
    world.setLeaf(21,2, visible)           
    world.setLeaf(21,4, visible)
                
lastSec = None
count = 0;

left = 12

def zehntel_sek():
    return round(time.time() * 1000)

start=zehntel_sek()

while (count < 6000):    
    now=zehntel_sek()   
    diff=now-start
    sec = int(diff / 1000) % 10
    if sec != lastSec:
        lastSec = sec
        paintDots()        

    paintNumber(int(diff / 10000) %10, left + 0)
    paintNumber(int(diff / 1000) % 10,   left + 4)
    paintNumber(int(diff / 100) % 10,   left + 10)
    
    tools.sleep(10);
    count+=1  