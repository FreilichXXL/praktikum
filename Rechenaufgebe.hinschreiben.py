zufallszahl_1 = tools.random(9)
zufallszeichen = tools.random(2)
zufallszahl_2 = tools.random(9)
ergebnis_richtig = 0
ergebnis_user = 0

if zufallszeichen == 0:
    ergebnis_richtig = zufallszahl_1 + zufallszahl_2
elif zufallszeichen == 1:
    ergebnis_richtig = zufallszahl_1 - zufallszahl_2
elif zufallszeichen == 2:
    ergebnis_richtig = zufallszahl_1 * zufallszahl_2
    
def umdrehen():
    kara.turnLeft()
    kara.turnLeft()

def positionieren():
    kara.move()
    kara.move()
    kara.move()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    umdrehen()

def null_schreiben():
    kara.turnRight()
    kara.putLeaf()
    kara.move()
    kara.turnLeft() 
    lange_strecke_kurve(True)
    kurze_strecke_kurve(True)
    lange_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.turnLeft()
    
def eins_schreiben():           
    kara.turnLeft()
    kara.move()
    kara.turnRight()
    lange_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()

def zwei_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnLeft()
    kara.move()
    umdrehen()
    
def drei_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnLeft()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.move()
    umdrehen()
    
def vier_schreiben():
    kara.turnRight()
    kara.move()
    kara.turnLeft()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.removeLeaf()
    kurze_strecke_kurve(True)
    kara.putLeaf()
    kara.move()
    kara.turnLeft()
    kara.move()
    kara.move()
    kara.turnLeft()
    
def fuenf_schreiben():
    kara.turnLeft()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    
def sechs_schreiben():
    kara.turnLeft()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(True)
    lange_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kurze_strecke_kurve(True)
    kara.turnLeft()
    kara.move()
    
def sieben_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()
    
def acht_schreiben():
    kara.turnRight()
    kara.move()
    umdrehen()
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.turnRight()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.putLeaf()
    kara.turnRight()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.turnLeft()
    
def neun_schreiben():
    kara.move()
    kara.move()
    kara.putLeaf()
    kara.turnRight()
    kara.move()
    kara.turnRight()
    kurze_strecke_kurve(False)
    kurze_strecke_kurve(False)
    lange_strecke_kurve(False)
    kurze_strecke_kurve(False)
    kara.putLeaf()
    kara.move()
    kara.move()
    kara.turnRight()
    kara.move()
   
def kurze_strecke_kurve(links):
    if links:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnLeft()
    else:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnRight()

def lange_strecke_kurve(links):
    if links:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnLeft()
    else:
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.putLeaf()
        kara.move()
        kara.turnRight()
        
def minus_schreiben():
    kara.move()
    kara.move()
    kara.putLeaf()
    kara.turnLeft()
    kara.move()
    kara.putLeaf()
    umdrehen()
    kara.move()
    kara.move()
    umdrehen()
    kara.putLeaf()
    kara.move()
    
def plus_schreiben():
    kara.move()
    kara.putLeaf()
    kara.move()
    kara.putLeaf()
    kara.move()
    kara.putLeaf()
    umdrehen()
    kara.move()
    kara.turnRight()
    kara.move()
    kara.putLeaf()
    umdrehen()
    kara.move()
    kara.move()
    umdrehen()
    kara.putLeaf()
    kara.move()
    
def mal_schreiben():
    kara.move()
    kara.move()
    kara.putLeaf()
    kara.turnLeft()
    
if zufallszahl_1 == 0: 
    null_schreiben()
elif zufallszahl_1 == 1:
    eins_schreiben()
elif zufallszahl_1 == 2:
    zwei_schreiben()
elif zufallszahl_1 == 3:
    drei_schreiben()
elif zufallszahl_1 == 4:
    vier_schreiben()
elif zufallszahl_1 == 5:
    fuenf_schreiben()
elif zufallszahl_1 == 6:
    sechs_schreiben()
elif zufallszahl_1 == 7:
    sieben_schreiben()
elif zufallszahl_1 == 8:
    acht_schreiben()
elif zufallszahl_1 == 9:
    neun_schreiben()
positionieren()
if zufallszeichen == 0: 
    plus_schreiben()
elif zufallszeichen == 1:
    minus_schreiben()
elif zufallszeichen == 2:
    mal_schreiben()
positionieren()
if zufallszahl_2 == 0:
    null_schreiben()
elif zufallszahl_2 == 1:
    eins_schreiben()
elif zufallszahl_2 == 2:
    zwei_schreiben()
elif zufallszahl_2 == 3:
    drei_schreiben()
elif zufallszahl_2 == 4:
    vier_schreiben()
elif zufallszahl_2 == 5:
    fuenf_schreiben()
elif zufallszahl_2 == 6:
    sechs_schreiben()
elif zufallszahl_2 == 7:
    sieben_schreiben()
elif zufallszahl_2 == 8:
    acht_schreiben()
elif zufallszahl_2 == 9:
    neun_schreiben()
    
kara.move()
kara.move()
tools.sleep(5000)
ergebnis_user = tools.intInput("Hier bitte dein Ergebnis eingeben")
while not ergebnis_user == ergebnis_richtig:
    ergebnis_user = tools.intInput("Das war leider falsch! Bitte nochmal eingeben")
tools.showMessage("das war richtig")
kara.move()
kara.turnLeft()
kara.move()
kara.turnRight()
kurze_strecke_kurve(False)
kara.putLeaf()
kara.move()
kara.move()
kara.turnRight()
kurze_strecke_kurve(False)
kara.putLeaf()
kara.move()
kara.turnRight()
kara.move()
positionieren()
ergebnis = ergebnis_richtig
if ergebnis == 0:
    null_schreiben()
    positionieren()
    null_schreiben()
elif ergebnis == 1:
    null_schreiben()
    positionieren()
    eins_schreiben()
elif ergebnis == 2:
    null_schreiben()
    positionieren()
    zwei_schreiben()
elif ergebnis == 3:
    null_schreiben()
    positionieren()
    drei_schreiben()
elif ergebnis == 4:
    null_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == 5:
    null_schreiben()
    positionieren()
    fuenf_schreiben()
elif ergebnis == 6:
    null_schreiben()
    positionieren()
    sechs_schreiben()
elif ergebnis == 7:
    null_schreiben()
    positionieren()
    sieben_schreiben()
elif ergebnis == 8:
    null_schreiben()
    positionieren()
    acht_schreiben()
elif ergebnis == 9:
    null_schreiben()
    positionieren()
    neun_schreiben()
elif ergebnis == 10:
    eins_schreiben()
    positionieren()
    null_schreiben()
elif ergebnis == 11:
    eins_schreiben()
    positionieren()
    eins_schreiben()
elif ergebnis == 12:
    eins_schreiben()
    positionieren()
    zwei_schreiben()
elif ergebnis == 13:
    eins_schreiben()
    positionieren()
    drei_schreiben()
elif ergebnis == 14:
    eins_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == 15:
    eins_schreiben()
    positionieren()
    fuenf_schreiben()
elif ergebnis == 16:
    eins_schreiben()
    positionieren()
    sechs_schreiben()
elif ergebnis == 17:
    eins_schreiben()
    positionieren()
    sieben_schreiben()
elif ergebnis == 18:
    eins_schreiben()
    positionieren()
    acht_schreiben()
elif ergebnis == 20:
    zwei_schreiben()
    positionieren()
    null_schreiben()
elif ergebnis == 21:
    zwei_schreiben()
    positionieren()
    eins_schreiben()
elif ergebnis == 24:
    zwei_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == 25:
    zwei_schreiben()
    positionieren()
    fuenf_schreiben()
elif ergebnis == 27:
    zwei_schreiben()
    positionieren()
    sieben_schreiben()
elif ergebnis == 28:
    zwei_schreiben()
    positionieren()
    acht_schreiben()
elif ergebnis == 30:
    drei_schreiben()
    positionieren()
    null_schreiben() 
elif ergebnis == 32:
    drei_schreiben()
    positionieren()
    zwei_schreiben() 
elif ergebnis == 35:
    drei_schreiben()
    positionieren()
    funef_schreiben() 
elif ergebnis == 36:
    drei_schreiben()
    positionieren()
    sechs_schreiben() 
elif ergebnis == 40:
    vier_schreiben()
    positionieren()
    null_schreiben() 
elif ergebnis == 42:
    vier_schreiben()
    positionieren()
    zwei_schreiben()
elif ergebnis == 45:
    vier_schreiben()
    positionieren()
    fuenf_schreiben()
elif ergebnis == 48:
    vier_schreiben()
    positionieren()
    acht_schreiben()
elif ergebnis == 49:
    vier_schreiben()
    positionieren()
    neun_schreiben()
elif ergebnis == 54:
    fuenf_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == 56:
    fuenf_schreiben()
    positionieren()
    sechs_schreiben()
elif ergebnis == 63:
    sechs_schreiben()
    positionieren()
    drei_schreiben()
elif ergebnis == 64:
    sechs_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == 72:
    sieben_schreiben()
    positionieren()
    zwei_schreiben()
elif ergebnis == 81:
    acht_schreiben()
    positionieren()
    eins_schreiben()
elif ergebnis == -1:
    minus_schreiben()
    positionieren()
    eins_schreiben()
elif ergebnis == -2:
    minus_schreiben()
    positionieren()
    zwei_schreiben()
elif ergebnis == -3:
    minus_schreiben()
    positionieren()
    drei_schreiben()
elif ergebnis == -4:
    minus_schreiben()
    positionieren()
    vier_schreiben()
elif ergebnis == -5:
    minus_schreiben()
    positionieren()
    fuenf_schreiben()
elif ergebnis == -6:
    minus_schreiben()
    positionieren()
    sechs_schreiben()
elif ergebnis == -7:
    minus_schreiben()
    positionieren()
    sieben_schreiben()
elif ergebnis == -8:
    minus_schreiben()
    positionieren()
    acht_schreiben()
elif ergebnis == -9:
    minus_schreiben()
    positionieren()
    neun_schreiben()    
kara.move()
kara.move()   
kara.turnRight()
tools.sleep(7000)    
world.clearAll()
kara.setPosition(1,0)
kara.turnRight()