def invertieren():
    if kara.onLeaf():
        kara.removeLeaf()
    else:
        kara.putLeaf()
def kurve(links):
    global kurven
    if links:
        kara.turnLeft()
        invertieren()
        kara.move()
        kara.turnLeft()
        kurven += 1
    else:
        kara.turnRight()
        invertieren()
        kara.move()
        kara.turnRight()
        kurven += 1
def gehen(): 
    global breite
    global schritte       
    while not breite == schritte:
        invertieren()
        kara.move()
        schritte += 1
    schritte = 0    
while True:       
    hoehe = world.getSizeY() - 1
    breite = world.getSizeX() - 1
    schritte = 0
    kurven = 0
    ende = hoehe / 2
    durchlaeufe = -1
    while not durchlaeufe == ende:
        while not hoehe == kurven:
            gehen()
            break
        while not hoehe == kurven:
            kurve(False)
            break
        while not hoehe == kurven: 
            gehen()
            break
        while not hoehe == kurven:
            kurve(True)
            break
        durchlaeufe += 1
    gehen()
    invertieren()
    kara.setPosition(0,0)
    break